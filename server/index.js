const path = require('path')
require('dotenv').config({ path: path.resolve(__dirname, '..', '.env') })

const mongoose = require('mongoose')
const { MONGO_USERNAME, MONGO_PASSWORD, MONGO_HOST, MONGO_PORT, MONGO_DATABASE } = process.env
mongoose.connect(
    `mongodb://${MONGO_USERNAME}:${MONGO_PASSWORD}@${MONGO_HOST}:${MONGO_PORT}/${MONGO_DATABASE}`,
    { useNewUrlParser: true }
)
mongoose.connection.once('open', () => {
    console.log('conneted to database')
})

const express = require('express')
const { createServer } = require('http')
const cors = require('cors')
const cookieParser = require('cookie-parser')

const { SubscriptionServer } = require('subscriptions-transport-ws')
const { execute, subscribe } = require('graphql')
const graphqlHTTP = require('express-graphql')
const schema = require('./schema/schema')

const PORT = 4000
const app = express()

app.use(cookieParser())

app.use(
    cors({
        origin: 'http://localhost:1234',
        credentials: true
    })
)

app.use('/graphql', (req, res) =>
    graphqlHTTP({
        schema,
        graphiql: true,
        context: { req, res }
    })(req, res)
)

const server = createServer(app)

server.listen(PORT, () => {
    console.log(`now listening for requests on port ${PORT}`)

    new SubscriptionServer(
        { execute, subscribe, schema },
        {
            server,
            path: '/subscriptions'
        }
    )
})
