const mongoose = require('mongoose')

const schema = new mongoose.Schema({
    name: String,
    uid: String,
    room: String,
    lastActive: Number
})

module.exports = mongoose.model('User', schema)
