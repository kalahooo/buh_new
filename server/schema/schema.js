const graphql = require('graphql')

const { PubSub, withFilter } = require('graphql-subscriptions')
const pubsub = new PubSub()

const Room = require('../models/room')
const User = require('../models/user')
const uid = require('uid')

const {
    GraphQLObjectType,
    GraphQLString,
    GraphQLSchema,
    GraphQLID,
    GraphQLList,
    GraphQLNonNull,
    GraphQLInt
} = graphql

const RoomType = new GraphQLObjectType({
    name: 'Room',
    fields: () => ({
        id: { type: GraphQLID },
        name: { type: GraphQLString }
    })
})

const UserType = new GraphQLObjectType({
    name: 'User',
    fields: () => ({
        uid: { type: GraphQLID },
        name: { type: GraphQLString },
        room: { type: GraphQLID },
        lastActive: { type: GraphQLInt }
    })
})

const USER_JOIN_ROOM = 'USER_JOIN_ROOM'
const USER_LEAVE_ROOM = 'USER_LEAVE_ROOM'

const Query = new GraphQLObjectType({
    name: 'Query',
    fields: {
        rooms: {
            type: new GraphQLList(RoomType),
            async resolve(parent, args, context) {
                await User.updateOne(
                    { uid: context.req.cookies.uid },
                    { room: null, lastActive: new Date().getTime() }
                )
                return Room.find({})
            }
        },
        usersByRoom: {
            type: new GraphQLList(UserType),
            args: {
                id: { type: GraphQLID }
            },
            async resolve(parent, args, context) {
                const uid = context.req.cookies.uid
                const room = args.id

                await User.updateOne({ uid }, { room, lastActive: new Date().getTime() })
                pubsub.publish(USER_JOIN_ROOM, { uid, room })

                return User.find({ room, uid: { $ne: uid } })
            }
        }
    }
})

const Mutation = new GraphQLObjectType({
    name: 'Mutation',
    fields: {
        addRoom: {
            type: RoomType,
            args: {
                name: { type: new GraphQLNonNull(GraphQLString) }
            },
            resolve(parent, args) {
                return Room.create({
                    name: args.name
                })
            }
        },
        removeRoom: {
            type: RoomType,
            args: {
                id: { type: GraphQLID }
            },
            resolve(parent, args) {
                return Room.findByIdAndDelete(args.id)
            }
        },
        userLogin: {
            type: UserType,
            args: {
                name: { type: GraphQLString }
            },
            resolve(parent, args) {
                const id = uid()
                return User.create({ name: args.name, uid: id, lastActive: new Date().getTime() })
            }
        },
        userLeaveRoom: {
            type: UserType,
            async resolve(parent, args, context) {
                const uid = context.req.cookies.uid

                pubsub.publish(USER_LEAVE_ROOM, { uid })

                return User.updateOne({ uid }, { room: null, lastActive: new Date().getTime() })
            }
        },
        userCloseRoom: {
            type: UserType,
            async resolve(parent, args, context) {
                const user = await User.findOne({ uid: context.req.cookies.uid })
                await Room.findByIdAndDelete(user.room)
                return User.updateOne(
                    { uid: context.req.cookies.uid },
                    { room: null, lastActive: new Date().getTime() }
                )
            }
        }
    }
})

const Subscription = new GraphQLObjectType({
    name: 'Subscription',
    fields: {
        userJoinRoom: {
            type: UserType,
            subscribe: () => pubsub.asyncIterator(USER_JOIN_ROOM),
            resolve: payload => payload
        },
        userLeaveRoom: {
            type: UserType,
            subscribe: () => pubsub.asyncIterator(USER_LEAVE_ROOM),
            resolve: payload => payload
        }
    }
})

module.exports = new GraphQLSchema({
    query: Query,
    mutation: Mutation,
    subscription: Subscription
})
