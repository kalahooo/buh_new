import React from 'react'
import styled from 'styled-components'
import { getUserMedia } from 'adapterjs'

const Wrap = styled.div`
    position: relative;
    border: 1px solid #3c93d5;
    box-shadow: 3px 3px 0 #3c93d5;
    overflow: hidden;
`
const VideoStyled = styled.video`
    position: absolute;
    left: -5%;
    top: -5%;
    width: 110%;
    height: 110%;
`
const DummyStyled = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    font-size: 2em;
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    background: #fff;
`

class Video extends React.PureComponent {
    constructor() {
        super()
        this.setRef = this.setRef.bind(this)
        this.state = {
            enabled: false
        }
    }

    componentWillUnmount() {
        if (this.stream) {
            const tracks = this.stream.getTracks() || []

            tracks.forEach(track => {
                track.stop()
            })
        }
    }

    componentDidMount() {
        const { self, stream } = this.props

        new Promise((resolve, reject) => {
            if (self) {
                getUserMedia(
                    { video: true, audio: true },
                    str => {
                        this.el.srcObject = str
                        this.stream = str
                        resolve()
                    },
                    reject
                )
            } else if (stream) {
                this.el.srcObject = stream
                resolve()
            } else {
                reject()
            }
        })
            .then(() => {
                setTimeout(() => {
                    this.setState({ enabled: true })
                }, 2000)
            })
            .catch(() => {
                console.log('no stream')
            })
    }

    setRef(el) {
        this.el = el
    }

    render() {
        const { enabled } = this.state
        return (
            <Wrap>
                <VideoStyled ref={this.setRef} autoPlay muted />
                {!enabled && <DummyStyled>•••</DummyStyled>}
            </Wrap>
        )
    }
}

export default Video
