import React from 'react'
import styled from 'styled-components'

export const Button = styled.button`
    border: none;
    border-radius: 3px;
    padding: 1.5rem 1rem;
    font-size: 22px;
    text-decoration: none;
    background-color: #55acee;
    box-shadow: 3px 3px 0px #3c93d5;
    color: #fff;
    &:hover {
        background-color: #6fc6ff;
    }
    &:active {
        transform: translate(2px, 2px);
        box-shadow: 1px 1px 0px #3c93d5;
    }
`
