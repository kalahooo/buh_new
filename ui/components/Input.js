import React from 'react'
import styled from 'styled-components'

const InputStyled = styled.input`
    border-radius: 0px;
    padding: 1.5rem 1rem;
    font-size: 22px;
    text-decoration: none;
    color: #55acee;
    box-shadow: inset 0px 0px 0px #3c93d5;
    background: #f4f4f4;
    border: none;
    max-width: 100%;
`

export const Input = props => <InputStyled {...props} />
