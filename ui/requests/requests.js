// import { gql } from 'apollo-boost'
import gql from 'graphql-tag'

export const ROOMS_GET_QUERY = gql`
    {
        rooms {
            name
            id
        }
    }
`

export const ROOM_REMOVE_MUTATION = gql`
    mutation RemoveRoom($id: ID) {
        removeRoom(id: $id) {
            id
            name
        }
    }
`

export const ROOM_ADD_MUTATION = gql`
    mutation AddRoom($name: String!) {
        addRoom(name: $name) {
            id
            name
        }
    }
`

export const USERS_GET_BY_ROOM_QUERY = gql`
    query UsersByRoom($id: ID) {
        usersByRoom(id: $id) {
            uid
            name
        }
    }
`

export const USER_JOIN_ROOM_MUTATION = gql`
    mutation UserJoinRoom($room: ID) {
        userJoinRoom(room: $room) {
            uid
            name
        }
    }
`

export const USER_LEAVE_ROOM_MUTATION = gql`
    mutation UserLeaveRoom {
        userLeaveRoom {
            uid
            name
        }
    }
`

export const USER_CLOSE_ROOM_MUTATION = gql`
    mutation UserCloseRoom {
        userCloseRoom {
            uid
            name
        }
    }
`

export const USER_LOGIN_MUTATION = gql`
    mutation UserLogin($name: String) {
        userLogin(name: $name) {
            uid
            name
        }
    }
`

export const USER_JOIN_ROOM_SUBSCRIPTION = gql`
    subscription {
        userJoinRoom {
            uid
            room
            name
        }
    }
`

export const USER_LEAVE_ROOM_SUBSCRIPTION = gql`
    subscription {
        userLeaveRoom {
            uid
            room
            name
        }
    }
`