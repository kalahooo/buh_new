import React from 'react'
import styled from 'styled-components'
import Cookies from 'js-cookie'
import { CenteredLayout } from '../layouts/CenteredLayout'
import { Input } from '../components/Input'
import { Button } from '../components/Button'
import { Mutation } from 'react-apollo'
import { USER_LOGIN_MUTATION } from '../requests/requests'
import { withRouter } from 'react-router-dom'

const StyledForm = styled.form`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: space-between;
    border: 1px solid #3c93d5;
    box-shadow: 3px 3px 0 #3c93d5;
    padding: 3rem;
    ${Button} {
        margin-top: 1.5rem;
    }
`
export const Auth = withRouter(({ history }) => (
    <Mutation mutation={USER_LOGIN_MUTATION}>
        {userLogin => (
            <CenteredLayout>
                <StyledForm
                    onSubmit={ev => {
                        ev.preventDefault()
                        const name = ev.currentTarget.elements[0].value
                        userLogin({ variables: { name } })
                            .then(({ data: { userLogin: { uid } } }) => {
                                if (uid) {
                                    Cookies.set('uid', uid)
                                    history.replace('/')
                                }
                            })
                            .catch(e => console.log(e))
                    }}
                >
                    <Input placeholder="Ваше имя" />
                    <Button>Войти</Button>
                </StyledForm>
            </CenteredLayout>
        )}
    </Mutation>
))
