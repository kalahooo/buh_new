import React from 'react'
import styled from 'styled-components'
import Cookies from 'js-cookie'
import { MainLayout } from '../layouts/MainLayout'
import { Button } from '../components/Button'
import Video from '../components/Video'
import {
    USER_CLOSE_ROOM_MUTATION,
    USER_LEAVE_ROOM_MUTATION,
    USERS_GET_BY_ROOM_QUERY,
    USER_JOIN_ROOM_SUBSCRIPTION,
    USER_LEAVE_ROOM_SUBSCRIPTION
} from '../requests/requests'
import { Mutation, Query } from 'react-apollo'
import { withRouter } from 'react-router-dom'
import { Loader } from '../components/Loader'

const Videos = styled.div`
    display: flex;
    flex: 1;
    justify-content: space-between;

    & > * {
        margin-bottom: 2rem;
        width: 300px;
        height: 210px;
    }
`

const Controls = styled.div`
    ${Button} {
        margin: 0 1rem 1rem 0;
    }
`

const Sidebar = withRouter(({ history }) => (
    <Controls>
        <Mutation mutation={USER_LEAVE_ROOM_MUTATION}>
            {userLeaveRoom => (
                <Button onClick={() => userLeaveRoom().then(() => history.replace('/'))}>
                    Выйти
                </Button>
            )}
        </Mutation>
        <Mutation mutation={USER_CLOSE_ROOM_MUTATION}>
            {userLeaveRoom => (
                <Button onClick={() => userLeaveRoom().then(() => history.replace('/'))}>
                    Закрыть комнату
                </Button>
            )}
        </Mutation>
    </Controls>
))

const Content = ({ users = [] }) => {
    return (
        <Videos>
            <Video self />
            {users.map(u => (
                <Video key={u.uid} />
            ))}
        </Videos>
    )
}

class Room extends React.Component {
    constructor() {
        super()
        this.subscribtions = []
    }
    componentWillUnmount() {
        this.subscribtions.forEach(s => s())
    }

    render() {
        const { match } = this.props

        return (
            <Query
                query={USERS_GET_BY_ROOM_QUERY}
                variables={{ id: match.params.id }}
                fetchPolicy="no-cache"
            >
                {({ loading, data, subscribeToMore }) => {
                    if (this.subscribtions.length === 0) {
                        this.subscribtions.push(
                            subscribeToMore({
                                document: USER_JOIN_ROOM_SUBSCRIPTION,
                                updateQuery(prev, { subscriptionData }) {
                                    const { usersByRoom = [] } = data

                                    if (
                                        subscriptionData.data ||
                                        subscriptionData.data.userJoinRoom.uid !==
                                            Cookies.get('uid')
                                    ) {
                                        return {
                                            ...data,
                                            usersByRoom: [
                                                ...usersByRoom,
                                                subscriptionData.data.userJoinRoom
                                            ]
                                        }
                                    }
                                }
                            })
                        )
                        this.subscribtions.push(
                            subscribeToMore({
                                document: USER_LEAVE_ROOM_SUBSCRIPTION,
                                updateQuery(prev, { subscriptionData }) {
                                    const { usersByRoom = [] } = data

                                    if (
                                        subscriptionData.data ||
                                        subscriptionData.data.userJoinRoom.uid !==
                                            Cookies.get('uid')
                                    ) {
                                        return {
                                            ...data,
                                            usersByRoom: usersByRoom.filter(
                                                u =>
                                                    u.uid !== subscriptionData.data.userJoinRoom.uid
                                            )
                                        }
                                    }
                                }
                            })
                        )
                    }
                    return (
                        <MainLayout
                            sidebarComponent={<Sidebar />}
                            contentComponent={
                                loading ? <Loader /> : <Content users={data.usersByRoom} />
                            }
                        />
                    )
                }}
            </Query>
        )
    }
}
const w = withRouter(Room)
export { w as Room }
