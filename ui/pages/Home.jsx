import React from 'react'
import styled from 'styled-components'
import { MainLayout } from '../layouts/MainLayout'
import { Button } from '../components/Button'
import Video from '../components/Video'
import { Mutation } from 'react-apollo'
import { ROOMS_GET_QUERY, ROOM_REMOVE_MUTATION, ROOM_ADD_MUTATION } from '../requests/requests'
import { Input } from '../components/Input'
import { Query } from 'react-apollo'
import { Link } from '../components/Link'

const SidebarStyled = styled.div`
    display: flex;
    flex-direction: column;
`

const LinkStyled = styled(Link)`
    margin: 1rem 0 !important;
    display: block;
`

const Room = ({ name, id }) => (
    <LinkStyled to={`/r/${id}`}>
        <Button>{name}</Button>
    </LinkStyled>
)

const CreateRoom = () => (
    <Mutation mutation={ROOM_ADD_MUTATION} refetchQueries={[{ query: ROOMS_GET_QUERY }]}>
        {addRoom => (
            <Input
                onKeyPress={ev => {
                    if (ev.key === 'Enter') {
                        addRoom({ variables: { name: ev.target.value } })
                        ev.target.value = ''
                        ev.target.blur()
                    }
                }}
            />
        )}
    </Mutation>
)

const Sidebar = () => (
    <Query query={ROOMS_GET_QUERY} fetchPolicy="no-cache">
        {({ loading, error, data }) => (
            <SidebarStyled>
                <CreateRoom />
                {data.rooms && data.rooms.map(d => <Room {...d} key={d.id} />)}
            </SidebarStyled>
        )}
    </Query>
)

const Content = <div> -- Выберите комнату или создайте свою</div>

export const Home = () => <MainLayout sidebarComponent={<Sidebar />} contentComponent={Content} />
