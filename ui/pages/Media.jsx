import React from 'react'
import styled from 'styled-components'
import Cookies from 'js-cookie'
import { CenteredLayout } from '../layouts/CenteredLayout'
import { Input } from '../components/Input'
import { Button } from '../components/Button'
import { getUserMedia } from 'adapterjs'

const StyledForm = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: space-between;
    border: 1px solid #3c93d5;
    box-shadow: 3px 3px 0 #3c93d5;
    padding: 3rem;
    ${Button} {
        margin-top: 1.5rem;
    }
    h1 {
        color: #3f94d8;
        text-align: center;
    }
`
export class Media extends React.PureComponent {
    componentDidMount() {
        this.checkCamera()
    }

    componentWillUnmount() {
        if (this.stream) {
            const tracks = this.stream.getTracks() || []

            tracks.forEach(track => {
                track.stop()
            })
        }
    }

    checkCamera() {
        Cookies.remove('cam')

        getUserMedia(
            { video: true, audio: true },
            stream => {
                Cookies.set('cam', 1)
                this.stream = stream
                this.props.history.replace('/')
            },
            () => {
                Cookies.remove('cam')
            }
        )
    }

    render() {
        return (
            <CenteredLayout>
                <StyledForm>
                    <h1>
                        Для работы с сайтом
                        <br />
                        необходим доступ к вашей веб-камере
                    </h1>
                    <p>Шаг 1. Убедитесь, что камера подключена</p>
                    <p>
                        Шаг 2. Когда появится запрос, выберите <strong>Разрешить</strong> или{' '}
                        <strong>Allow</strong>
                    </p>
                    <p>
                        Шаг 3. Если запрос не появляется, перезагрузите страницу или попробуйте
                        нажать на кнопку:
                    </p>
                    <Button onClick={() => this.checkCamera()}>Попробовать еще</Button>
                    <p>
                        Шаг 4. Если ничего не происходит, посмотрите, нет ли в адресной строке
                        значка <img src={require('../assets/blocked-cam.png')} />
                    </p>
                    <p>
                        Шаг 5. Нажмите на значок и в открывшемся окне выберите{' '}
                        <strong>Разрешить</strong> или <strong>Allow</strong>
                    </p>
                    <p>Шаг 6. Вернитесь к 1 шагу</p>
                </StyledForm>
            </CenteredLayout>
        )
    }
}
