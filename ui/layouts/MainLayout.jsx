import React from 'react'
import styled from 'styled-components'

const Container = styled.div`
    display: flex;
    margin: auto;
    width: 96rem;

    @media (max-width: 640px) {
        width: 100%;
        flex-direction: column;
    }
`

const Sidebar = styled.div`
    width: 24rem;
    padding-right: 4rem;

    @media (max-width: 640px) {
        width: 100%;
        display: flex;
        padding: 0;
    }
`

const Content = styled.div`
    display: flex;
    flex: 1;
`

export const MainLayout = ({ sidebarComponent, contentComponent }) => (
    <Container>
        <Sidebar>{sidebarComponent}</Sidebar>
        <Content>{contentComponent}</Content>
    </Container>
)
