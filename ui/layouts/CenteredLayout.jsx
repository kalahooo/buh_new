import React from 'react'
import styled from 'styled-components'

const Container = styled.div`
    display: flex;
    margin: auto;
    width: 96rem;
    height: 100%;
    align-items: center;
    justify-content: center;
`

export const CenteredLayout = ({ children }) => (
    <Container>
        {children}
    </Container>
)
