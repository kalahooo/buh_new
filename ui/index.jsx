import React from 'react'
import { render } from 'react-dom'
// import ApolloClient from 'apollo-boost'
import ApolloClient from 'apollo-client'
import { ApolloProvider } from 'react-apollo'

import { InMemoryCache } from 'apollo-cache-inmemory'
import { split } from 'apollo-link'
import { HttpLink } from 'apollo-link-http'
import { WebSocketLink } from 'apollo-link-ws'
import { getMainDefinition } from 'apollo-utilities'

const PORT = 4000

// using the ability to split links, you can send data to each link
// depending on what kind of operation is being sent
const link = split(
    // split based on operation type
    ({ query }) => {
        const { kind, operation } = getMainDefinition(query)
        return kind === 'OperationDefinition' && operation === 'subscription'
    },
    new WebSocketLink({
        uri: `ws://localhost:${PORT}/subscriptions`,
        options: {
            reconnect: true
        }
    }),
    new HttpLink({
        uri: `http://localhost:${PORT}/graphql`,
        credentials: 'include'
    })
)

const cache = new InMemoryCache()

const client = new ApolloClient({
    link,
    cache
})

import { App } from './app'

const Root = () => (
    <ApolloProvider client={client}>
        <App />
    </ApolloProvider>
)

render(<Root />, document.getElementById('app'))
