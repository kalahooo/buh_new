import React from 'react'
import { createGlobalStyle } from 'styled-components'
import { BrowserRouter as Router, Route, Redirect } from 'react-router-dom'
import Cookies from 'js-cookie'

import { Home } from './pages/Home'
import { Room } from './pages/Room'
import { Auth } from './pages/Auth'
import { Media } from './pages/Media'

const GlobalStyle = createGlobalStyle`
    html {
        font-size: 10px;
    }
    body {
        font-size: 1.6rem;
        font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
        margin: 0;
    }
    html, body, #app, #app > * {
        height: 100%;
        min-height: 100%;
    }
    button, a, select, input { outline: none; }
    button { cursor: pointer; }
    
    h1 { font-size: 1.5em;}
`

function isLogged() {
    return Cookies.get('uid')
}

function isAllowedCamera() {
    return Cookies.get('cam')
}


export const App = () => (
    <React.Fragment>
        <Router>
            <React.Fragment>
                <Route
                    path="/"
                    exact
                    render={() => (isLogged() ? <Home /> : <Redirect to="/auth" />)}
                />
                <Route
                    path="/r/:id"
                    render={() => {
                        if (!isLogged()) {
                            return <Redirect to="/auth" />
                        } else if (!isAllowedCamera()) {
                            return <Redirect to="/m" />
                        } else {
                            return <Room />
                        }
                    }}
                />
                <Route path="/auth" render={() => (isLogged() ? <Redirect to="/" /> : <Auth />)} />
                <Route path="/m" component={Media} />
            </React.Fragment>
        </Router>
        <GlobalStyle />

    </React.Fragment>
)